'use strict';


angular.module('mfisheries.CoastguardServices',[])

.service("getTrackDates", function($http, LocalStorage){
	
	var deffered = {};

	deffered.get = function(userid){
		// If user not specified then retrieve information from logged in user
        if (!userid) {
            console.log("user not specified then retrieve information from logged in user");
            userid = LocalStorage.getObject('user').userid;
        }

		console.log('api/get/track/dates'+'/'+userid);
		return $http.get('api/get/track/dates'+'/'+userid);
	};

	return deffered;
})

.service("getTracksByDay", function($http, LocalStorage){

	var deffered = {};

	deffered.get = function(day,month,year, userid){
		// If user not specified then retrieve information from logged in user
		if (!userid) {
			console.log("user not specified then retrieve information from logged in user");
            userid = LocalStorage.getObject('user').userid;
        }

		console.log('api/get/track/by/day/'+day+'/'+month+'/'+year+'/'+userid);
		return $http.get('api/get/track/by/day/'+day+'/'+month+'/'+year+'/'+userid);
	};

	return deffered;
})


.service("getUserTracks", function($http, LocalStorage){

	var deffered = {};

	deffered.get = function(day,month,year,userid,starttrackid){
		// If user not specified then retrieve information from logged in user
		if (!userid) {
            console.log("user not specified then retrieve information from logged in user");
            userid = LocalStorage.getObject('user').userid;
        }

		// console.log('api/get/user/tracks/'+day+'/'+month+'/'+year+'/'+userid+'/'+starttrackid+'/'+userid);
		return $http.get('api/get/user/tracks/'+day+'/'+month+'/'+year+'/'+userid+'/'+starttrackid+'/'+userid);
	};

	return deffered;
})


.service("resolveSos", function($http){
	var deffered = {};

	deffered.post = function(data){

		return $http.post('/api/update/sos/status', data);
	};
	return deffered;
});

