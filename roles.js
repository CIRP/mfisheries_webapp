// TODO How to load this information from a database instead of hard coding in the script
angular.module('mfisheries')
	.constant('USER_ROLES', {
		admin: {
			name: 'Administrator',
			code: 2,
			description: "The technical administrator for the mFisheries installation. Has oversight over the entire system.",
			alias: "Tech Admin",
			scope: {visibility: 'global', privilege: 'admin'},
			default_path: '/admin/users'
		},
		coastguard: {
			name: 'Coast Guard',
			code: 1,
			description: "Account for Safety at Sea organization. Displays information critical to responding to Fishers in distress at sea.",
			scope: {visibility: 'country', privilege: 'rescue'},
			default_path: '/all/tracks'
		},
		user: {
			name: 'Public User',
			code: 3,
			description: "The end-user for system. Fishers and mobile app users belong to this group",
			alias: "Fisherfolk",
			scope: {visibility: 'country', privilege: 'basic'},
			default_path: '/image/diary'
		},
		reviewer: {
			name: 'Reviewer',
			code: 4,
			description: "A global read only user.",
			scope: {visibility: 'global', privilege: 'read-only'},
			default_path: '/admin/users'
		},
		country_reviewer: {
			name: "Country Reviewer",
			code: 5,
			description: "A read only user for information of a specific",
			scope: {visibility: 'country', privilege: 'read-only'},
			default_path: '/admin/users'
		},
		country_admin: {
			name: "Country Administrator",
			code: 6,
			description: "The administrative user that configures modules and manage data for specific countries.",
			alias: "Country Admin",
			scope: {visibility: 'country', privilege: 'admin'},
			default_path: '/admin/users'
		},
		fewer_admin: {
			name: "Agency Administrator",
			code: 7,
			description: "The administrator that will manage fishers within an agency.",
			alias: "Agency Admin",
			scope: {visibility: 'agency', privilege: 'admin'},
			default_path: '/admin/users'
		},
		fewer_kiosk: {
			name: "Data Consumer",
			code: 8,
			description: "The user that will simply consume basic information.",
			alias: "Kiosk",
			scope: {visibility: 'country', privilege: 'read-only'},
			default_path: '/admin/users'
		},
		fisher_organization: {
			name: "Organization User",
			code: 9,
			description: "The user that will be able to view agency related information",
			alias: "Member",
			scope: {visibility: 'agency', privilege: 'basic'},
			default_path: '/dashboard'
		},
		technical_admin: {
			name: "Technical Administrator",
			code: 10,
			description: "The user that will be able to view agency related information",
			alias: "Member",
			scope: {visibility: 'country', privilege: 'admin'},
			default_path: '/image/diary'
		}
	});