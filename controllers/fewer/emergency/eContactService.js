// angular.module('mfisheries.EmergencyModule')
// .service("EmergencyContact", ["$http", function($http){
// 	var emergencyContact = {};
//
// 	emergencyContact.get = function(countryid){
// 		if (countryid && countryid !== 0)
// 			return $http.get('/api/emergencycontact', {
// 				params: {"countryid": countryid}
// 			});
// 		else
// 			return $http.get('/api/emergencycontact');
// 	};
//
// 	emergencyContact.add = function(contact){
// 		return $http.post('/api/emergencycontact', contact);
// 	};
//
// 	emergencyContact.delete = function(contact){
// 		return $http.delete('/api/emergencycontact/'+contact.id);
// 	};
//
// 	emergencyContact.update = function(contact){
// 		return $http.put('/api/emergencycontact/'+contact.id, contact);
// 	};
//
// 	return emergencyContact;
// }]);

class EmergencyContact extends BaseAPIService{
	constructor($http){
		super($http, "api/emergencycontact");
	}
}
EmergencyContact.$inject = [
	"$http"
];
angular.module('mfisheries.EmergencyModule')
	.service("EmergencyContact", EmergencyContact);