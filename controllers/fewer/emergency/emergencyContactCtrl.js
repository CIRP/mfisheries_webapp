"use strict";

/**
 * class for the Emergency Contact Controller
 *
 */
class EmergencyContactCtrl{
	constructor(LocalStorage, Country, EmergencyContact, AuthService, Locator){
		console.log("Created Emergency Contact Controller");
		this.LocalStorage = LocalStorage;
		this.Country = Country;
		this.EmergencyContact = EmergencyContact;
		this.AuthService = AuthService;
		this.locator = Locator;
		
		this.userCountry = 0;
		this.records = [];
		this.init();
	}
	
	init(){
		console.log("Initializing the Emergency Controller");
		// Check if user is signed in
		this.AuthService.attachCurrUser(this).then(currUser => {
			if (currUser)console.log("User is logged in");
			else console.log("User is not logged in");
		});
		// Set Boolean to control notification of loading data
		this.loading_data = false;
	}
	
	startView(){
		console.log("Starting View for Emergency contacts");
		// Retrieve countries to be displayed to the user
		this.displayLoading("Loading Countries").then(loading =>{
			// After Displaying notification
			this.retrieveCountries(true).then(countries =>{
				loading.close();
				this.locator.getLastLocationID().then(countryid => {
					console.log("Location found from cache as: " + countryid);
					if (countryid && countryid !== "undefined")this.handleCountryChange(countryid);
				});
			}, err => loading.close());
		});
	}
	
	retrieveData(countryid){
		this.loading_data = true;
		if (!countryid)countryid = this.userCountry;
		this.EmergencyContact.get(countryid).then(res => {
			console.log("Retrieved %s contacts from countryid %s", res.data.length, this.userCountry);
			this.records.length = 0;
			this.records = res.data.map(el => {
				if (el.additional && el.additional.length > 1) {
					el.additionalJSON = JSON.parse(el.additional);
				} else {
					el.additionalJSON = [];
				}
				this.loading_data = false;
				return el;
			});
			console.log(this.records);
		}, err => console.error(err));
	}

	retrieveCountries(display) {
		return new Promise((resolve, reject) => {
			const self = this;
			this.Country.get(this.userCountry).then(res => {
				self.countries = res.data;
				if (display) this.countries.unshift({ id: 0, name: "Select Country"});
				resolve(res.data);
			}, reject);
		});
	}
	
	handleCountryChange(countryid){
		console.log("Country was changed to:" + countryid);
		if (!isNaN(countryid))countryid = parseInt(countryid);
		this.userCountry = countryid;
		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve data
		this.retrieveData(countryid);
	}

	/**
	 * Displays the a control-less loading message dialog.
	 * Clients that use the function should use the extent and call close
	 * method on the response to dismiss message
	 * @param message
	 * @returns {Promise}
	 */
	displayLoading(message){
		swal({
			type: "info",
			title: "Weather",
			text: message,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showConfirmButton: false,
			onOpen: function () {
				swal.showLoading();
			}
		});

		return Promise.resolve(swal);
	}
}

EmergencyContactCtrl.$inject = [
	'LocalStorage',
	'Country',
	'EmergencyContact',
	'AuthService',
	'Locator'
];

angular.module('mfisheries.EmergencyModule')
	.controller('EmergencyContactCtrl', EmergencyContactCtrl);