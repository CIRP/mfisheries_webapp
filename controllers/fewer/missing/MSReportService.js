
class MSReportService extends BaseAPIService{
	constructor($http){
		super($http, "/api/missingpersons");
	}
}

MSReportService.$inject = [
		"$http"
];


class MSReportFieldService extends BaseAPIService{
	constructor($http){
		super($http,"/api/missingpersons/fields");

	}
}

angular.module("mfisheries.MissingPersons")
		.service("MSReportService",MSReportService)
		.service("MSReportFieldService",MSReportFieldService);