/**
 * Locator class is used to manage operations involved in retrieval of the user's location
 */
class Locator {
	/**
	 * Constructor to create and initialize properties of Locator class
	 * @param LocalStorage The localstorage service for mFisheries system
	 */
	constructor(LocalStorage) {
		this.locate_options = {};
		this.localStorage = LocalStorage;
		this.geolocator = window.geolocator;
		this.init();
	}
	
	/**
	 * Performs additional initialization operations.
	 * In this class:
	 * 1. It sets up the configuration of the gelocator library
	 * 2. Assigns default values used to retrieve location
	 */
	init() {
		this.geolocator.config({
			language: "en",
			google: {
				version: "3",
				key: "AIzaSyCf_c-22sJUAtnmV-RZoImFGrCnTM1dXQQ"
			}
		});
		
		this.locate_options = {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumWait: 10000,
			maximumAge: 0,
			desiredAccuracy: 30,
			fallbackToIP: true,
			addressLookup: true,
			timezone: true
		};
	}
	
	/**
	 *
	 * @returns {Promise}
	 */
	retrieveLastLocation() {
		return new Promise((resolve, reject) => {
			if (this.localStorage.hasKey("location"))
				resolve(this.localStorage.getObject("location"));
			else
				reject("Location was not previous received");
		});
	}
	
	
	/**
	 * Retrieves the current location by checking the cache first and then attempting to retrieve from
	 * @returns {Promise}
	 */
	retrieveCurrentLocation() {
		return new Promise((resolve, reject) => {
			// TODO Implement Time To Live
			if (this.localStorage.hasKey("location")) {
				const location = this.localStorage.getObject("location");
				console.log("received from cache: " + location);
				resolve(location);
				return;
			}
			console.log("attempting to retrieve location from system");
			this.geolocator.locate(this.locate_options, (err, loc) => {
				if (err) reject(err); // if we are unable to retrieve location pass on the reason
				else {
					this.localStorage.setObject("location", loc);
					resolve(loc);
				}
			});
		});
	}

	resolveAddressToLocation(loc){
		return new Promise((resolve, reject) =>{

		});
	}

	/**
	 *
	 * @param id
	 * @returns {Promise.<{key: countryid, value: id}>}
	 */
	saveLastLocationID(id){
		return this.localStorage.save("countryid", id);
	}

	/**
	 * Retrieves the countryid stored within the system
	 * @returns {Promise.<countryid>}
	 */
	getLastLocationID(){
		return this.localStorage.get("countryid", true);
	}
}

Locator.$inject = [
	"LocalStorage"
];
angular.module('mfisheries.services').service("Locator", Locator);