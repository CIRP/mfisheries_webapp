"use strict";
// userProfileCtrl.js
angular.module('mfisheries.User')
	.controller('userProfileCtrl', function ($scope, LocalStorage, $routeParams,
	                                         UserProfile, Country, Villages, Occupations, AuthService) {
		
		$(".mFish_menu").removeClass("active");
		$("#menu-profile").addClass("active");
		$("#menu-user").addClass("active");


        console.log("User Profile Controller");
		// Load the User From Server and Populate Fields
		$scope.user = {};
		$scope.vessel = "no";
		$scope.readOnly = false;
		let user, countryID;
		
		AuthService.attachCurrUser($scope);
		const currUser = $scope.currentUser;
		
		let uid = currUser.userid;
		console.log("Logged in User:" + uid);
		
		if ($routeParams.userid) {
			if (currUser.userRole === 2 || currUser.userRole === 1 || currUser.userRole === 4) {
				uid = $routeParams.userid;
				$scope.readOnly = true;
				console.log("Changed user profile requested to: " + uid);
			}
		}
		
		UserProfile.get(uid).then(function (res) {
			const rec = UserProfile.convert(res.data.data);
			user = rec.user;
			countryID = rec.selectedCountry;
			// Retrieve Countries
			Country.get().then(function (res) {
				$scope.countries = res.data;
				// When Country Completed Retrieve Occupations
				Occupations.get(countryID).then(function (res) {
					$scope.occupations = res.data;
					
					//When Occupation Completed Retrieve Villages
					Villages.get(countryID).then(function (res) {
						$scope.villages = res.data;
						
						//After Villages loaded we set the data in the scope
						$scope.user = user;
						$scope.selectedCountry = countryID;
						
						if (user.standard.vessel) {
							$scope.vessel = "yes";
						}
					});
				});
			});
		});
		
		$scope.onCountryChange = function (countryid) {
			Occupations.get(countryid).then(function (res) {
				$scope.occupations = res.data.data;
			});
			Villages.get(countryid).then(function (res) {
				$scope.villages = res.data.data;
			});
		};
		
		$scope.submit = function () {
			$scope.submitted = true;
			
			$.each($scope.user, function (index, data) {
				console.log("Processing: " + index);
				if ($scope.user[index]) {
					// For Each of the items within the user object (retrieved from the form) we will add the user id
					$scope.user[index].userid = uid;
					if (index === 'basic') {
						// Use jquery to get the selected items
						const occ = [];
						$(".occupation_options").filter(":checked").each(function (i, el) {
							console.log($(el).val());
							occ.push($(el).val());
						});
						$scope.user[index].occupations = occ;
					}
					if (index === 'standard') {
						console.log($scope.vessel);
						if ($scope.user[index].vessel && $scope.user[index].vessel === "yes") {
							$scope.user[index].hasvessel = "yes";
						} else {
							$scope.user[index].hasvessel = "no";
						}
					}
				}
			});
			
			UserProfile.post($scope.user).then(function (res) {
				swal({
					title: "Success!!",
					text: "Your Account has been created!",
					type: "success"
				});
			});
			return false;
		};
		
	});