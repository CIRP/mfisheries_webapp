class CountryModule extends BaseAPIService{
	constructor($http){
		super($http, "/api/country/modules");
	}
}

CountryModule.$inject = [
	"$http"
];

angular
	.module('mfisheries.AdminServices', [])
	.service("CountryModule", CountryModule);