class Country extends BaseAPIService{
	constructor($http){
		super($http, "/api/countries");
	}
}

Country.$inject = [
	"$http"
];
angular.module('mfisheries.Country').service("Country", Country);