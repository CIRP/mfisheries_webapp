class CountryLoc extends BaseAPIService{
	constructor($http){
		super($http, "/api/countrylocs");
	}
}

CountryLoc.$inject = [
	"$http"
];
angular.module('mfisheries.Country').service("CountryLoc", CountryLoc);