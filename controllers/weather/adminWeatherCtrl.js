"use strict";

angular.module('mfisheries.Weather', [])
		.controller('adminWeatherCtrl', [
			'$scope', 'LocalStorage', '$compile', 'fileupload', 'Country', 'Module', 'CountryModule', 'WeatherSource', 'Threshold', 'Entries', 'AuthService',
			function ($scope, LocalStorage, $compile, fileupload, Country, Module, CountryModule, WeatherSource, Threshold, Entries, AuthService) {
				console.log("Weather Controller");

				$(".mFish_menu").removeClass("active");
				$("#menu-weather").addClass("active");

				AuthService.attachCurrUser($scope);
				const currUser = $scope.currentUser;

				$scope.sourcedata = [];
				$scope.weatherSources = [];
				$scope.countries = [];

				$scope.sourceNames = [
					currUser.country + " - MET Office",
					currUser.country + "-NEMO",
					currUser.country + "-ODM",
					"NOAA",
					"CIMH",
					"OpenWeather",
					"Weather Underground",
					"AccuWeather"
				];

				$scope.reset = function () {
					$scope.newSource = {
						"source": "",
						"countryid": currUser.countryid,
						"infotype": "Weather",
						"url": "",
						"frequency": "Every Hour",
						"sourcetype": "Website",
						"duration": "Single Day",
						'createdby': currUser.userid
					};
				};
				
				//Set the form to its original values when page is loaded
				$scope.reset();

				$scope.weatherSources = [];
				$scope.state = " ";

				$scope.addSource = function () {
					$scope.state = "add";
					$scope.reset();
					$("#weatherModal").modal('show');
				};

				$scope.showExtractor = function () {
					$("#ExtractorUploadModal").modal('show');
				};

				$scope.newType = function () {
					$("newType").removeClass("hidden");
				};

				$scope.setTime = function () {
					$("#time").removeClass('hidden');
					$("#times").removeClass('hidden');
				};

				$scope.testExtractor = function (weather) {
					console.log(weather);
					
					const data = {
						"id": weather.id,
						"codepath": weather.codepath
					};
					
					//
					Entries.test(data).then(function (res) {
						console.dir(res);
						if (res.data) {
							swal("Record Added", "Record was successfully Added", "success");
							refresh();
						} else swal("Failed", "Issue sent to relevant authority", "error");
					}, err => {
						swal("Entries", "Unable to retrieve data from extractor", "error");
					});
				};

				$scope.deleteSource = function (source) {
					swal({
						title: "Delete Confirmation",
						text: "Are you sure you want to delete this record. You will not be able to undo this operation",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Delete"
					}).then(result => {
						if (result) {
							Entries.delete(source).then((res) => {
								console.log("Entries deleted for source: " + source);
								Threshold.delete(source).then((res) => {
									console.log("Successfully deleted threshold" + res);

									WeatherSource.delete(source).then(function (res) {
										console.log(res);

										swal("Deleted", "Record was successfully deleted", "success");
										refresh();

									}, function (err) {
										console.log(err);
										swal("Failed", "Unable to delete record", "error");
									});
								}, (err) => {
									console.log("Unable to delete treshold records" + err);
								});
							}, (err) => {
								console.log("Unable to delete weather records" + err);
							});
						}else console.log("Request to delete record was cancelled");


					});
				};

				function loadWeather() {
					$scope.loading = true;
					console.log($scope.userCountry);
					console.log("Data");
					WeatherSource.get($scope.userCountry).then(function (res) {
						if (res.status === 200) {
							$scope.weatherSources = res.data;
							console.log("Retrieved " + $scope.weatherSources.length + "source records");
							$scope.loading = false;
						}
					});
				}

				$scope.type = "";
				$scope.readings = [];

				function findID(weatherID) {

					for (let i = 0; i < ($scope.weatherSources.length); i++) {
						console.log(i);
						if ($scope.weatherSources[i].id === weatherID) {
							return $scope.weatherSources[i];
						}
					}
				}
				
				function displayLoading(title, message){
					swal({
						type: "info",
						title: title,
						text: message,
						allowOutsideClick: true,
						allowEscapeKey: true,
						showConfirmButton: false,
						onOpen: function () {
							swal.showLoading();
						}
					});
					
					return Promise.resolve(swal);
				}
				
				// TODO Review loadData due to inoperability in the system currently

				$scope.loadData = function (weather) {
					$("#viewModal").modal("show");
					const title = "Retrieving Extractor Data";
					const msg = "Retrieving data for:" + weather.name;
					// Display an indicator to user that information is requested
					// displayLoading(title, msg).then(loading => {
						// Entries.get(weather.countryid).then();
                        Entries.get(weather.countryid).then(function (res) {
                        	console.log(res.data);
                        	if (res.data) {
                        		const data = res.data.filter(el => el.sourceid === weather.id)[0];
                        		if (data && data.readings) {
                        			$scope.readings = data.readings;
                        			delete $scope.readings.sourceid;
                        			console.log("Retrieved " + $scope.readings.length + "weather records");

                        			if ($scope.type === "Sensor") {
                        				sensorReadings($scope.readings);
                        				console.log($scope.readings);
                        			}
                        		}else{
                        			swal("Weather Extractor", "Unable to display received readings", "error");
                        		}
                        		console.log($scope.readings.date);
                        	}else{
                        		swal("Weather Extractor", "No readings for the extractor received", "error");
                        	}
                        }, err => {
                        	swal("Weather Extractor", "No readings for the extractor received", "error");
                        });

					// });
					
					// Entries.get(weather.countryid).then(function (res) {
					// 	console.log(res);
					// 	if (res.data) {
					// 		const data = res.data.filter(el => el.sourceid === weather.id)[0];
					// 		if (data && data.readings) {
					// 			$scope.readings = data.readings;
					// 			delete $scope.readings.sourceid;
					// 			console.log("Retrieved " + $scope.readings.length + "weather records");
					//
					// 			if ($scope.type === "Sensor") {
					// 				sensorReadings($scope.readings);
					// 				console.log($scope.readings);
					// 			}
					// 		}else{
					// 			swal("Weather Extractor", "Unable to display received readings", "error");
					// 		}
					// 		console.log($scope.readings.date);
					// 	}else{
					// 		swal("Weather Extractor", "No readings for the extractor received", "error");
					// 	}
					// }, err => {
					// 	swal("Weather Extractor", "No readings for the extractor received", "error");
					// });
				};

				function anyReadings(readings){
				
				}
				
				function sensorReadings(readings) {
					console.log(readings);
					let html = "";
					for (let i = 0; i < (readings.date.length); i++) {
						html += '<tr>';
						for (let key in readings) {
							console.log(key);
							html += '<td>' + readings[key][i] + '</td>';
						}
						html += '</tr>';
					}
					$('#sensorTable').html(html);
				}

				function displayErrorFeedback(title, message) {

					$("#spinning_upload").hide();
					$("#weather_source_next_btn").removeAttr('disable');

					swal({
						title: title,
						type: "error",
						text: message
					});
				}

				$scope.uploadExtractor = function () {
					console.log("Button Pressed");
					const uploadUrl = '/api/weather/extractor';
					console.log($scope.newSource);
					if ($scope.state == "add") {
						if ($scope.extractor) { // Ensure File is Specified
							const sendData = {country: currUser.country};

							$("#weather_source_next_btn").attr('disable', 'true');
							$("#spinning_upload").show();

							// We have the file so we attempt to upload the extractor first
							fileupload.uploadFileToUrl($scope.extractor, sendData, uploadUrl).then(function (data) {
								console.log(data);
								// If Successful, then we Add the Source and Entry to the database
								if (data.data && data.data.status === 200) {
									// Save the path of the uploaded extractor to the source (Allows for management related tasks)
									$scope.newSource.codepath = data.data.extractor_path;

									let configFieldsJSON;
									if (data.data.fields) {
										console.log("Received Data Fields from server for uploaded extractor");
										configFieldsJSON = data.data.fields;
										console.log(configFieldsJSON);
									} else { // If no configuration fields provided by extractor
										console.log("No Configurable fields for extractor was specified");
									}


									console.log("Attempting to save weather source with the following information");
									console.dir($scope.newSource);

									// Save the Weather Source
									WeatherSource.add($scope.newSource).then(function (weatherRes) {
												console.log("Weather result: ");
												console.log(weatherRes);

												$("#spinning_upload").hide();
												if (weatherRes.data && weatherRes.status === 201) {
													console.log('success uploading weather source ... moving to configuration');
													// update the source data with the id of the saved record
													$scope.newSource.id = weatherRes.data.id;

													console.log("Successfully saved weather source ... moving to setting the thresholds");

													// Display Threshold configuration based on fields received from upload process
													$scope.fields = configFieldsJSON;
													$scope.field_names = Object.keys(configFieldsJSON);

													$scope.resetThresholds(configFieldsJSON);

													let template = $("#config_fields_section").html();
													let content = $compile(template)($scope);
													$('#weather_source_sec').html(content);

												} else {
													displayErrorFeedback("Adding New Weather Source", "Unable to add new weather source. If problem persists contact support to report error");
												}
											},
											function () { // Error Callback if uploading weather source fails
												displayErrorFeedback("Adding New Weather Source", "Unable to add new weather source. If problem persists contact support to report error");
											});

								} else { // The upload extractor process failed
									displayErrorFeedback("Uploading Extractor", "Unable to Upload Extractor File. Ensure python file follows the implementation guidelines");
								}
							});
						} else { // File is not Specified so Display message to user
							displayErrorFeedback("Uploading Module", "No extractor file uploaded. Add extractor and resubmit.");
						}
					}
					else if ($scope.state === "edit") {
						if ($scope.extractor) { // Ensure File is Specified
							const sendData = {country: currUser.country};

							$("#weather_source_next_btn").attr('disable', 'true');
							$("#spinning_upload").show();

							// We have the file so we attempt to upload the extractor first
							fileupload.uploadFileToUrl($scope.extractor, sendData, uploadUrl).then(function (data) {
								console.log(data);
								// If Successful, then we Add the Source and Entry to the database
								if (data.data && data.data.status === 200) {
									// Save the path of the uploaded extractor to the source (Allows for management related tasks)
									$scope.newSource.codepath = data.data.extractor_path;

									let configFieldsJSON;
									if (data.data.fields) {
										console.log("Received Data Fields from server for uploaded extractor");
										configFieldsJSON = data.data.fields;
										console.log(configFieldsJSON);
									} else { // If no configuration fields provided by extractor
										console.log("No Configurable fields for extractor was specified");
									}


									console.log("Attempting to save weather source with the following information");
									console.dir($scope.newSource);

									// Save the Weather Source
									WeatherSource.update($scope.newSource).then(function (weatherRes) {
												console.log("Weather result: ");
												console.log(weatherRes);

												$("#spinning_upload").hide();
												if (weatherRes.data && (weatherRes.status === 201 || weatherRes.status === 200)) {
													console.log('success uploading weather source ... moving to configuration');
													// update the source data with the id of the saved record
													$scope.newSource.id = weatherRes.data.id;

													console.log("Successfully saved weather source ... moving to setting the thresholds");

													// Display Threshold configuration based on fields received from upload process
													$scope.fields = configFieldsJSON;
													$scope.field_names = Object.keys(configFieldsJSON);

													$scope.resetThresholds(configFieldsJSON);

													let template = $("#config_fields_section").html();
													let content = $compile(template)($scope);
													$('#weather_source_sec').html(content);

												} else {
													displayErrorFeedback("Adding New Weather Source", "Unable to add new weather source. If problem persists contact support to report error");
												}
											},
											function () { // Error Callback if uploading weather source fails
												displayErrorFeedback("Adding New Weather Source", "Unable to add new weather source. If problem persists contact support to report error");
											});

								} else { // The upload extractor process failed
									displayErrorFeedback("Uploading Extractor", "Unable to Upload Extractor File. Ensure python file follows the implementation guidelines");
								}
							});
						} else { // File is not Specified so Display message to user

							WeatherSource.update($scope.newSource).then(function (res) {
								console.log('Updated Source');
								if (res.status === 200) {
									$('#weatherModal').modal('hide');
									swal('Updating Source', 'Successfully Updated Source', 'success');
								}
								else {
									swal('Updating Source', 'Error Updating Source', 'error');
								}
							});
						}
					}
				};

				function loadCountries() {
					$scope.country_loading = true;
					Country.get($scope.userCountry).then(function (res) {
							console.log($scope.userCountry);
							$scope.countries = res.data;
							console.log($scope.countries);
							$scope.country_loading = false;
					});
				}

				function detectCountry() {
					console.log("Detect Country");
					const loggedInUser = currUser;
					if (loggedInUser) {
						$scope.newSource.country = loggedInUser.country;
						$scope.newSource.source = loggedInUser.country + " - MET Office";
						// uncomment to show for the country user is logged in
						// $scope.userCountry = loggedInUser.countryid;
					}
					console.log(loggedInUser);
				}

				function refresh() {
					detectCountry();
					loadWeather();
					loadCountries();
				}

				refresh();
				// Threshold related functions
				$scope.thresholds = {};

				$scope.thresholds = function () {
					$("#thresholdModal").modal('show');
				};

				$scope.resetThresholds = function (fields) {
					$scope.thresholds = {
						'warnings': {},
						'emergency': {}
					};
				};
				$scope.thresh = {};

				$scope.edit = {};
				
				$scope.editSource = function (weather) {
					$scope.newSource = {};
					$scope.newSource = weather;
					$scope.state = "edit";
					$("#weatherModal").modal('show');
				};
				
				$scope.resetThresholds();

				$scope.saveThresholds = function (threshold) {
					if ($scope.state == "add") {
						let rec = {
							thresholds: $scope.thresholds,
							weathersourceid: $scope.newSource.id
						};
						console.log("Adding here");
						Threshold.add(rec).then(function (res) {
							console.log("Successfully added a new threshold for " + rec.weathersourceid);
							swal('Adding New Source', 'Entry Successfully Added', 'success');

							location.reload();
						}, function (err) {
							console.log(rec);
							console.log(err);
						});
					} else {
						console.log($scope.newSource);
						let rec = {
							thresholds: $scope.thresholds,
							weathersourceid: $scope.newSource.id
						};
						Threshold.update(rec).then(function (res) {
							console.log("Successfully added a new threshold for " + rec.weathersourceid);
							swal('Adding New Source', 'Entry Successfully Added', 'success');

							location.reload();
						}, function (err) {
							console.log(rec);
							console.log(err);
						});
					}
				};
			}]);