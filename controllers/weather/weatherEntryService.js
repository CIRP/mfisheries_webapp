class Entries extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/entry/weather");
	}
	
	/**
	 *
	 * @param data
	 */
	test(data){
		return this.$http.post('/api/entry/manualrun', data);
	}
}

Entries.$inject = [
	"$http"
];
angular.module('mfisheries.Weather').service("Entries", Entries);

// angular.module('mfisheries.Weather')
// 	.service("Entries", ["$http", function($http){
// 		var weatherEntry = {};
//
// 		weatherEntry.post = function(data){
// 			return $http.post('/api/entry/weather', data);
// 		};
//
// 		weatherEntry.test = function(object){
// 			return $http.post('/api/entry/manualrun', object);
// 		};
// 		weatherEntry.get = function(id){
// 			if (id && id !== 0){
// 			return $http.get('/api/entry/weather/'+id);
// 			}
// 			return $http.get('/api/entry/weather');
//
// 		};
// 		weatherEntry.delete = function(id){
// 			return $http.delete('/api/entry/weather/'+id);
// 		};
// 		return weatherEntry;
// 	}]);