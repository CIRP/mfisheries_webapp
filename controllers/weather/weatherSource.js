
class WeatherSource extends BaseAPIService{
	constructor($http){
		super($http, "/api/weathersources");
	}
}

WeatherSource.$inject = [
	"$http"
];
angular.module('mfisheries.Weather').service("WeatherSource", WeatherSource);