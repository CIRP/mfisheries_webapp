// "use strict";
//
// angular.module('mfisheries.LekController', [])
//
// 	.controller('lekCtrl', [
// 		'$scope', '$q', 'LocalStorage', '$window', '$map', 'getLek', 'getLekByCategory', 'getLekByDate', 'getLekByRange', 'LEKCategory', 'Country', 'getLastImage', 'AuthService',
// 		function ($scope, $q, LocalStorage, $window, $map, getLek, getLekByCategory, getLekByDate, getLekByRange, LEKCategory, Country, getLastImage, AuthService) {
//
// 			$(".mFish_menu").removeClass("active");
// 			$("#menu-lek").addClass("active");
//
// 			// Configure operations for ACL
// 			AuthService.attachCurrUser($scope);
//
// 			const map = $("#map"),
// 				calendar = $('#calendar'),
// 				dates = [];
//
// 			console.log("Attempting to retrieve current location");
// 			$map.getLocation(location => {
// 				if (location) {
// 					console.log(location);
// 				} else {
// 					console.log("Unable to retrieve location");
// 				}
// 			});
//
// 			$map.initMap(map);
// 			$map.setType("LEK");
//
// 			$scope.typeFilter = "all";
// 			$scope.categoryFilter = "all";
// 			$scope.countryFilter = "default";
// 			$scope.leks = [];
// 			$scope.lek = "";
// 			$scope.offset = 0;
// 			$scope.dateFilter = new Date();
// 			$scope.rangeFilter = "week";
// 			$scope.advanced = false;
//
// 			const date = new Date();
// 			date.setDate(date.getDate() - 7);
//
// 			function loadCountries() {
// 				$scope.country_loading = true;
// 				Country.get($scope.userCountry).then(function (res) {
// 					$scope.countries = res.data;
// 					$scope.country_loading = false;
// 				});
// 			}
//
// 			function loadLEKCategories() {
// 				LEKCategory.get($scope.currentUser.userid).then(function (res) {
// 					$scope.categories = res.data.data;
// 					console.log("categories: " + JSON.stringify($scope.categories));
// 					$scope.lek_loading = false;
// 				});
// 			}
//
// 			loadCountries();
//
// 			getLekByRange.get(
// 				$scope.currentUser.userid, $scope.typeFilter,
// 				$scope.categoryFilter, $scope.countryFilter,
// 				date.getDate(), date.getMonth() + 1, date.getFullYear(),
// 				$scope.dateFilter.getDate(), $scope.dateFilter.getMonth() + 1,
// 				$scope.dateFilter.getFullYear()).then(function (res) {
// 				$scope.leks = res.data.data;
// 				$map.addLEKMarker(res.data.data);
// 			});
//
// 			$scope.getLEKByDate = function () {
// 				console.log("get lek by date ");
// 				getLekByDate.get(
// 					$scope.currentUser.userid, $scope.typeFilter,
// 					$scope.categoryFilter,
// 					$scope.dateFilter.getDate(),
// 					$scope.dateFilter.getMonth() + 1,
// 					$scope.dateFilter.getFullYear()).then(function (res) {
// 					$scope.leks = res.data.data;
// 					$map.addLEKMarker(res.data.data);
// 				});
// 			};
//
// 			$scope.getLEKByRange = function () {
// 				console.log("get lek by range");
// 				console.log($scope.typeFilter + " " + $scope.categoryFilter + " " + $scope.countryFilter);
// 				const today = new Date();
// 				const date = new Date();
// 				if ($scope.rangeFilter === "week") {
// 					console.log("by week");
// 					date.setDate(date.getDate() - 7);
// 				}
// 				else if ($scope.rangeFilter === "month") {
// 					console.log("by month");
// 					date.setMonth(date.getMonth() - 1);
// 				}
// 				else if ($scope.rangeFilter === "year") {
// 					console.log("by year");
// 					date.setFullYear(date.getFullYear() - 1);
// 				}
// 				getLekByRange.get($scope.currentUser.userid, $scope.typeFilter, $scope.categoryFilter, $scope.countryFilter,
// 					date.getDate(), date.getMonth() + 1, date.getFullYear(),
// 					today.getDate() + 1, today.getMonth() + 1, today.getFullYear()).then(function (res) {
// 					$scope.leks = res.data.data;
// 					$map.addLEKMarker(res.data.data);
// 				});
// 			};
//
// 			$scope.loadMore = function () {
// 				if ($scope.offset >= 0) {
// 					$scope.offset += 10;
// 					getLek.get($scope.currentUser.userid, $scope.typeFilter, $scope.offset).then(function (res) {
// 						if (res.data.data.length > 0) {
// 							$scope.leks = $scope.leks.concat(res.data.data);
// 							console.log($scope.leks.length);
// 						} else {
// 							$scope.offset = -1;
// 						}
// 					});
// 				} else {
//
// 				}
//
// 			};
//
// 			$scope.toggleAdvanced = function () {
// 				$scope.advanced = !$scope.advanced;
// 			};
//
// 			function dayClick(date, jsEvent, view) {
//
// 			}
//
// 			function eventClick(calEvent, jsEvent, view) {
// 				const s = calEvent.start._i.split('-'),
// 					d = new Date(parseInt(s[0]), parseInt(s[1]) - 1, parseInt(s[2]));
//
// 				getLastImage.get($scope.currentUser.userid, d.getDate(), d.getMonth() + 1, d.getFullYear()).then(function (res) {
// 					console.log(res.data.data);
// 					$map.addImageMarker(res.data.data);
// 				});
//
// 				$map.zoomOut();
// 			}
//
// 		}]);


/**
 * Class for CAP listing controller.
 * The same class is used for both viewing and administrative functionality
 * Step 1 develop Retrieval for country dropdown list
 * Step 2 develop Retrieval for public group for country
 */
class LekCtrl extends BaseController {
	constructor(Country,Locator, AuthService) {
		super(Country, Locator, AuthService);
		this.init();
	}

	init() {
		super.init();
		this.reports = [];
		this.userCountry = 0;
	}

	startView() {
		super.startView().then(res =>{
			console.log(res);
		}, err => console.log(err));
	}

	handleCountryChange(countryid){
		console.log("Country was changed to:" + countryid);
		if (!isNaN(countryid))countryid = parseInt(countryid);
		this.userCountry = countryid;

		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve posts
	}


}

LekCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService"
];

angular.module('mfisheries.LEK', []).controller("LekCtrl", LekCtrl);